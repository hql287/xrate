ExRate is a simple CLI tool to convert currencies via [currencylayer](https://currencylayer.com/) API

Currencies data: https://gist.github.com/Fluidbyte/2973986
