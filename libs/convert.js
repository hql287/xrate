// Libs
const getExchangeRates = require('./getExchangeRates');
const currencies       = require('../data/currencies.json');
const ora              = require('ora');
const currencyFormatter = new Intl.NumberFormat();

// Validate input
const validateInput = (amount, sourceCurrency, targetCurrency) => {
  // Set currecny codes array to check
  const currencyCodes = Object.keys(currencies);
  const validateSpinner = ora('Validating input...').start();
  // Check if amount is a valid number
  if (isNaN(amount)) {
    validateSpinner.fail('Invalid Amount');
    return false;
  }
  // Check if souce currency is supported
  if (!currencyCodes.includes(sourceCurrency.toUpperCase())){
    validateSpinner.fail('Source currency is not supported');
    return false;
  }
  // Check if target currency is supported
  if (!currencyCodes.includes(targetCurrency.toUpperCase())) {
    validateSpinner.fail('Target currency is not supported');
    return false;
  }
  validateSpinner.succeed();
  return true;
}

const convert = (amount, sourceCurrency, targetCurrency) => {
  // 1. Validate Input
  if (!validateInput(amount, sourceCurrency, targetCurrency)) {
    return false;
  }

  // 2. Retrieve exchange rates
  const rateSpinner = ora('Getting Exchange Rates...').start();
  getExchangeRates()
    .then(data => {
      // 3. Convert the currency
      // Set the final exchange value
      let value;
      rateSpinner.succeed();
      const convertSpinner = ora('Converting...').start();
      // If USD IS THE source currency
      if (sourceCurrency === 'USD') {
        const exchangeRateCode = `${sourceCurrency.toUpperCase()}${targetCurrency.toUpperCase()}`;
        const exchangeRate     = data.quotes[exchangeRateCode];
        value                  = amount*exchangeRate;
      // If USD is NOT the sourceCurrency currency
      } else {
        // Convert sourceCurrency currency to USD
        const usdRateCode = `USD${sourceCurrency.toUpperCase()}`;
        const usdRate     = data.quotes[usdRateCode];
        const usdValue    = amount/usdRate;
        // Convert USD Value to the desired currency
        const exchangeRateCode = `USD${targetCurrency.toUpperCase()}`;
        const exchangeRate     = data.quotes[exchangeRateCode];
        value                  = usdValue*exchangeRate;
      }
      // Print out the final message
      convertSpinner.succeed();
      console.log(`${sourceCurrency} ${currencyFormatter.format(amount)} = ${targetCurrency} ${currencyFormatter.format(value)}`);
    });
}

module.exports = convert;
