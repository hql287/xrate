// Libraries
const axios = require('axios');
const api   = require('../data/api.json');

const getExchangeRates = () => {
  const url = `http://www.apilayer.net/api/live?access_key=${api.key}&format=1`;
  return axios.get(url)
    .then(response => {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
}

module.exports = getExchangeRates
