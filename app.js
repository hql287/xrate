#! /usr/bin/env node

// Import Lib
const convert = require('./libs/convert');

// Collecting Arguments
const [amount, source, finalCurrency] = process.argv.slice(2);

// Convert
convert(amount, source, finalCurrency);
